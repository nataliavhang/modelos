<?php

use Illuminate\Support\Facades\Route;
use El_Norte\Modelos\Greetr;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/greetr/{name}', function($sName) {
    $oGreetr = new Greetr();
    return $oGreetr->greet($sName);
});
